FROM bioconductor/bioconductor_docker:devel
 
MAINTAINER Michela Dell'Alma <michela.dellalma@icloud.com>
 
RUN apt-get clean all && \
  apt-get update && \
  apt-get upgrade -y && \
  apt-get install -y \
    libhdf5-dev \
    libcurl4-gnutls-dev \
    libssl-dev \
    libxml2-dev \
    libpng-dev \
    libxt-dev \
    zlib1g-dev \
    libbz2-dev \
    liblzma-dev \
    libglpk40 \
    libgit2-28 \
  && apt-get clean all && \
  apt-get purge && \
  rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
 
RUN Rscript -e "install.packages(c('rmarkdown', 'tidyverse', 'Seurat', 'patchwork', 'viridis', 'ggforce', 'ggridges', 'devtools', 'writexl'))"

RUN R -e 'BiocManager::install(ask=F)' && R -e 'BiocManager::install(c("scDblFinder", "SingleR", "scran", "GenomeInfoDb", "GenomicRanges", "GenomicFeatures", "Rsamtools", "plyranges", "sa-lee/analysis-superintronic", "rtracklayer", ask=F))'

RUN R -e 'devtools::install_github("erocoar/gghalves")'
