# r_scrnaseq

Docker file to build a container with packages useful to:
- run a scRNAseq analysis,
- run an analysis to find coverage in introns and exons regions


## Build the image:

`docker build -t <username>/<repo>:<tag> . `

<username> is your Dockerhub user name 

<repo> is the name of the repository where you will store the Docker (which can be an existing repo or the name of a repo that will be created when you push the Docker to Dockerhub) 

<tag> is a keyword or version number that you want to attach to identify a specific image. 

The mark ‘.’ at the end of the command, tells docker to build in the working directory 

## Test that it works:

`docker run -d --rm -p 28787:8787 --name hello-world rstudio/hello-world `

in this case we have used the flag -d because we want to run it as background service 

1. access Rstudio by typing in the browser yourhostip:28787 

2. username and password are both rstudio 

Or run interactively 

docker run -it <username>/<repo>:<tag> 

if you want to access a bash shell or other interactive mode, specify -it 
-rm the option remove makes the thing such that the container remains alive as long as we you use it, it’s important \ 
to make a container as “ephemeral” as possible (stopped, destroyed, rebuilt and replaced easily, i.e. with an absolute \ 
minimum set up and configuration) 

if you want to access a bash shell or other interactive mode, specify -it 

-rm: the option remove makes the thing such that the container remains alive as long as we you use it, it’s important \ 
to make a container as “ephemeral” as possible (stopped, destroyed, rebuilt and replaced easily, i.e. with an absolute \ 
minimum set up and configuration) 

## Share your docker image

First create a repository in docker hub with the same name of your name repository 

You can also change the name of your local repository by running this command
`docker tag local-image:tagname new-repo:tagname`
 
Push the repository in dockerhub
`docker push new-repo:tagname`

Make sure to change tagname with your desired image repository tag. 

## You can also convert the docker image into a singularity image
singularity pull docker://link_of_the_gitlab_repo
